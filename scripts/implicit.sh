#!/bin/bash

echo "Invalid client (GET, public)"
curl -ki "https://localhost:8443/oauth2/authorize?response_type=token&client_id=client_pub_implicit_invalid&redirect_uri=http://localhost/cb"  

echo "Unauthorized client (GET, public)"
curl -ki "https://localhost:8443/oauth2/authorize?response_type=token&client_id=client_pub_code&redirect_uri=http://localhost/cb"  

echo "Wrong URI (GET, public)"
curl -ki "https://localhost:8443/oauth2/authorize?response_type=token&client_id=client_pub_implicit&redirect_uri=http://somehost/cb"  

echo "Wrong URI (GET, public)"
curl -ki "https://localhost:8443/oauth2/authorize?response_type=token&client_id=client_pub_implicit&redirect_uri=https://somehost/cb"  

echo "OK (GET, public)"
curl -ki "https://localhost:8443/oauth2/authorize?response_type=token&client_id=client_pub_implicit&redirect_uri=http://localhost/cb" | grep grant=
GRANT=`grep "grant=" /tmp/response`
echo $GRANT

echo "OK (POST, public)"
curl -ki "https://localhost:8443/oauth2/authorize" -d "response_type=token" -d "client_id=client_pub_implicit" -d "redirect_uri=http://localhost/cb" > /tmp/response
