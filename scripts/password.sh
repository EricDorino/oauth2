#!/bin/bash

GRANT=grant_type=password
PASS=`cat ./scripts/password.txt`

echo "Invalid request (public)"
curl -ki https://localhost:8443/oauth2/token -d $GRANT -d username=$1 -d password=$2  

echo "Invalid client (public)"
curl -ki https://localhost:8443/oauth2/token -d $GRANT -d client_id=none -d username=$1 -d password=$2  

echo "Unauthorized client (public)"
curl -ki https://localhost:8443/oauth2/token -d $GRANT -d client_id=client_pub_implicit -d username=$1 -d password=$2  

echo "Access denied (public)"
curl -ki https://localhost:8443/oauth2/token -d $GRANT -d client_id=client_pub_password -d username=$1 -d password=xx  

echo "OK (public)"
curl -ki https://localhost:8443/oauth2/token -d $GRANT -d client_id=client_pub_password -d username=$1 -d password=$2  

echo "OK (confidential)"
curl -ki https://localhost:8443/oauth2/token -d $GRANT -d client_id=client_conf_all -d client_secret=$PASS -d username=$1 -d password=$2  

echo "OK (confidential Basic)"
curl -ki https://localhost:8443/oauth2/token -d "$GRANT" -u client_conf_all:$PASS -d "username=$1" -d "password=$2"  

echo "OK (confidential+scope)"
curl -ki https://localhost:8443/oauth2/token -d $GRANT -d client_id=client_conf_all -d client_secret=$PASS -d username=$1 -d password=$2 -d "scope=calendar p2p" 

