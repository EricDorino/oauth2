#!/bin/sh

RC="./src/oauth2_register"
CODEPWD="./scripts/code.pwd"
IMPLICITPWD="./scripts/implicit.pwd"
PASSWORDPWD="./scripts/password.pwd"
CLIENTPWD="./scripts/client.pwd"

sudo $RC -p -a code -u http://localhost/cb pub_code
sudo $RC -p -a implicit -u http://localhost/cb pub_implicit 
sudo $RC -p -a password -u http://localhost/cb pub_password
sudo $RC -p -a client -u http://localhost/cb pub_client

sudo $RC -c -a code -u http://localhost/cb conf_code > $CODEPWD
sudo $RC -c -a implicit -u http://localhost/cb conf_implicit > $IMPLICITPWD
sudo $RC -c -a password -u http://localhost/cb conf_password > $PASSWORDPWD
sudo $RC -c -a client -u http://localhost/cb conf_client > $CLIENTPWD
