#!/bin/bash

GRANT=grant_type=client_credentials
PASS=`cat ./scripts/password.txt`

echo "Invalid request (public)"
curl -ki https://localhost:8443/oauth2/token -d $GRANT

echo "Invalid client (public)"
curl -ki https://localhost:8443/oauth2/token -d $GRANT -d client_id=none -d client_secret=$PASS

echo "Unauthorized client (public)"
curl -ki https://localhost:8443/oauth2/token -d $GRANT -d client_id=client_pub_implicit -d client_secret=$PASS

echo "OK (confidential)"
curl -ki https://localhost:8443/oauth2/token -d $GRANT -d client_id=client_conf_all -d client_secret=$PASS

echo "OK (confidential Basic)"
curl -ki https://localhost:8443/oauth2/token -d $GRANT -u client_conf_all:$PASS 

echo "OK (confidential Basic with scope)"
curl -ki https://localhost:8443/oauth2/token -d $GRANT -u client_conf_all:$PASS  -d "scope=feeds"

