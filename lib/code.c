/*
 * Copyright (C) 2016  Éric Dorino.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "oauth2d.h"

char *oa2_encode(const char *str)
{
	char *result = NULL, *p;

	if (!str)
		return NULL;

	if (!(result = malloc(3*strlen(str)+1)))
		return NULL;

	p = result;

	while (*str) {
		if ((*str >= 'a' && *str <= 'z') ||
				(*str >= 'A' && *str <= 'Z') ||
				(*str >= '0' && *str <= '9') ||
				*str == '-' || *str == '_' ||
				*str == '.' || *str == '~')
			*p++ = *str++;	
		else {
			char val[3];

			sprintf(val, "%02x", (unsigned)*str++);
			*p++ = '%';
			*p++ = val[0];
			*p++ = val[1];
		}
	}
	*p = '\0';
	return result;
}

char *oa2_decode(const char *str)
{
	char *buf = NULL;
	const char *p = str;
	char *q;

	if (!str)
		return NULL;

	if (!(buf = malloc(strlen(str)+1)))
		return NULL;

	q = buf;

	while (*p) {
		if (*p == '%') {
			char c[3];

			p++;
			c[0] = *p++;
			c[1] = *p++;
			c[2] = '\0';
			*q++ = strtol(c, NULL, 16);
		}
		else
			*q++ = *p++;
	}
	*q = '\0';
	return buf;
}
