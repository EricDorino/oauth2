/*
 * Copyright (C) 2016  Éric Dorino.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include "oauth2d.h"

static int find(const char *token)
{
	struct oa2_token t;

	if (oa2_token_find(token, &t)) {
		printf("Found %s\n", token);
		printf("validity: %ld\nrefresh: %d\nclient_id: \"%s\"\n"
				 	 "user_id: \"%s\"\nscope: \"%s\"\n\n",
					 t.validity, t.refresh, t.client_id, t.user_id, t.scope);
		return 1;
	}
	else {
		if (errno < 0)
			perror("test_token");
		else
			printf("Not found %s\n\n", token);
		return 0;
	}
}

int main(void)
{
	
	printf("store: %d\n", oa2_token_store("TOKEN0", time(NULL), 0,
									"client_id0", "user_id0", NULL));
	printf("store: %d\n", oa2_token_store("TOKEN1", time(NULL)+15000, 0,
									"client_id0", "user_id0", NULL));
	printf("store: %d\n", oa2_token_store("TOKEN2", time(NULL), 0,
									"client_id0", "user_id0", "feeds"));
	printf("store: %d\n", oa2_token_store("TOKEN3", -1, 1,
									"client_id0", "user_id0", "feeds calendar"));

	find("INEXISTANT");
	find("TOKEN0");
	find("TOKEN1");
	find("TOKEN2");
	find("TOKEN3");

	printf("delete TOKEN2\n");
	oa2_token_delete("INEXISTANT");
	oa2_token_delete("TOKEN2");
	find("TOKEN2");

	sleep(5);
	printf("expire %lu\n", time(NULL));
	oa2_token_expire();
	find("TOKEN0");
	find("TOKEN1");
	find("TOKEN3");

	oa2_token_update("TOKEN3", time(NULL)+10000);
	find("TOKEN3");

	return EXIT_SUCCESS;
}
