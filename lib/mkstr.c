/*
 * Copyright (C) 2016  Éric Dorino.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <sys/file.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <wait.h>
#include "oauth2d.h"

int mkstr(char *buf, size_t bufsize, int type)
{
	pid_t pid;
	int fd[2], status;

	memset(buf, '\0', bufsize);

	if (pipe(fd) < 0)
		return 0;

	if ((pid = fork()) < 0)
		return 0;
	else if (pid == 0) {
		if (dup2(fd[1], STDOUT_FILENO) < 0)
			return 0;
		switch (type) {
#ifdef HAVE_PWGEN
			case MKSTR_PASSWORD:
				execl(PWGEN, PWGEN, "-sync", "32", "1", NULL);
				break;
			case MKSTR_SALT:
				execl(PWGEN, PWGEN, "-nc", "32", "1", NULL);
				break;
			case MKSTR_ID:
				execl(PWGEN, PWGEN, "-0A", "12", "1", NULL);
				break;
			case MKSTR_GRANT:
			case MKSTR_TOKEN:
				execl(PWGEN, PWGEN, "-nc", "32", "1", NULL);
				break;
#else
#error "No usable password generator."
#endif
			default:
				errno = EINVAL;
				return 0;
		}
		return 0;
	}
	close(fd[1]);
	wait(&status);
	if (read(fd[0], buf, bufsize) < 0)
		return 0;
	close(fd[0]);
	buf[strlen(buf)-1] = '\0';
	return 1;
}
