/*
 * Copyright (C) 2016  Éric Dorino.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include "oauth2d.h"

static char *str(int auth)
{
	static char buf[16];

	memset(buf, 0, sizeof(buf));
	if (auth == CODE_AUTH)
		strcpy(buf, "code");
	else if (auth == IMPLICIT_AUTH)
		strcpy(buf, "implicit");
	else if (auth == PASSWORD_AUTH)
		strcpy(buf, "password");
	else if (auth == CLIENT_AUTH)
		strcpy(buf, "client");
	else
		strcpy(buf, "?");
	return buf;
}

static int find(const char *client)
{
	struct oa2_client c;

	if (oa2_client_find(client, &c)) {
		int i;

		printf("Found %s\n", client);
		printf("confidential: %d\nauth: %s\nsecret: \"%s\"\nredirect_uri: ",
					 c.confidential,
					 str(c.auth),
					 c.secret);
		i = 0;
		while (c.redirect_uri[i]) {
			printf("\"%s\" ", c.redirect_uri[i]);
			i++;
		}
		putchar('\n');
		return 1;
	}
	else {
		if (errno > 0)
			perror("test_client");
		else
			printf("Not found \"%s\"\n\n", client);
		return 0;
	}
}

int main(int argc, char **argv)
{
	if (argc != 2) {
		fputs("usage: test_client <client_id>\n", stderr);
		return EXIT_FAILURE;
	}
	
	find(argv[1]);

	return EXIT_SUCCESS;
}
