/*
 * Copyright (C) 2016  Éric Dorino.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <sys/file.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include "oauth2d.h"

int oa2_token_find(const char *token, struct oa2_token *tok)
{
	FILE *f;
	char *buf, *p, *p1, *scope, *type, *validity;

	if (!token)
		return 0;

	if (!(f = fopen(OAUTH2_TOKENS, "r")))
		return 0;

	if (flock(fileno(f), LOCK_EX) < 0)
		return 0;

	while (fgets(tok->buf, sizeof(tok->buf), f)) {
		buf = tok->buf;
		p = strsep(&buf, ":");
		if (!p)
			goto not_found;
		if (strcmp(token, p) != 0)
			continue;
		errno = 0;

		validity = strsep(&buf, ":");
		tok->validity = (time_t)atol(validity);
		type = strsep(&buf, ":");
		if (strcmp(type, "refresh") == 0)
			tok->refresh = 1;
		else
			tok->refresh = 0;
		tok->client_id = strsep(&buf, ":");
		tok->user_id = strsep(&buf, ":");
		scope = strsep(&buf, "\n");
		if (strlen(scope) == 0)
			tok->scope = NULL;
		else {
			char *scope0 = oa2_decode(scope);

			strcpy(scope, scope0);
			free(scope0);
			tok->scope = scope;
		}

		flock(fileno(f), LOCK_UN);
		fclose(f);
		return 1;
	}

	not_found:
	errno = EINVAL;
	flock(fileno(f), LOCK_UN);
	fclose(f);
	return 0;
}

int oa2_token_store(const char *token,
							time_t validity,
							int refresh, 
							const char *client_id,
							const char *user_id,
							const char* scope)
{
	FILE *f;
	char *sc = NULL;

	if (!(f = fopen(OAUTH2_TOKENS, "a")))
		return 0;

	if (flock(fileno(f), LOCK_EX) < 0)
		return 0;

	if (scope)
		sc = oa2_encode(scope);

	fprintf(f, "%s:%ld:%s:%s:%s:%s\n",
		token, (long)validity,
		refresh ? "refresh" : "normal",
		client_id ? client_id : "", 
		user_id ? user_id : "",
		sc ? sc : "");

	flock(fileno(f), LOCK_UN);
	fclose(f);
	return 1;
}	

void oa2_token_update(const char *token, time_t validity)
{
	FILE *oldf, *newf;
	char buf[8192], *p;

	if (!(oldf = fopen(OAUTH2_TOKENS, "r")))
		return;
	if (!(newf = fopen(OAUTH2_TOKENS ".new", "w")))
		return;

	if (flock(fileno(oldf), LOCK_EX) < 0)
		return;

	while (fgets(buf, sizeof(buf), oldf)) {
		p = index(buf, ':');
		if (p && strncmp(token, buf, p-buf) == 0) {
			fprintf(newf, "%s:%lu", token, (long)validity);
			if ((p = index(p+1, ':')))
				fputs(p, newf);
			continue;
		}
		fputs(buf, newf);
	}
	flock(fileno(oldf), LOCK_UN);
	fclose(oldf);
	fclose(newf);
	rename(OAUTH2_TOKENS ".new", OAUTH2_TOKENS);
}

void oa2_token_delete(const char *token)
{
	FILE *oldf, *newf;
	char buf[8192], *p;

	if (!(oldf = fopen(OAUTH2_TOKENS, "r")))
		return;
	if (!(newf = fopen(OAUTH2_TOKENS ".new", "w")))
		return;

	if (flock(fileno(oldf), LOCK_EX) < 0)
		return;

	while (fgets(buf, sizeof(buf), oldf)) {
		p = index(buf, ':');
		if (p && strncmp(token, buf, p-buf) == 0)
			continue;
		fputs(buf, newf);
	}
	flock(fileno(oldf), LOCK_UN);
	fclose(oldf);
	fclose(newf);
	rename(OAUTH2_TOKENS ".new", OAUTH2_TOKENS);
}

void oa2_token_expire(void)
{
	FILE *oldf, *newf;
	char buf[8192], *p, *q;

	if (!(oldf = fopen(OAUTH2_TOKENS, "r")))
		return;
	if (!(newf = fopen(OAUTH2_TOKENS ".new", "w")))
		return;

	if (flock(fileno(oldf), LOCK_EX) < 0)
		return;

	while (fgets(buf, sizeof(buf), oldf)) {
		p = index(buf, ':');
		if (p && (q = index(p+1, ':'))) {
			char val[32];
			time_t validity;

			memset(val, 0, sizeof(val));
			strncpy(val, p+1, q-p);
			validity = atol(val);
			if (validity > 0 && validity < time(NULL))
				continue;
		}
		fputs(buf, newf);
	}
	flock(fileno(oldf), LOCK_UN);
	fclose(oldf);
	fclose(newf);
	rename(OAUTH2_TOKENS ".new", OAUTH2_TOKENS);
}
