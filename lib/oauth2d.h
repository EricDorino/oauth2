/*
 * Copyright (C) 2016,  Éric Dorino.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef lib_oauth2d_h_
#define lib_oauth2d_h_

#include <time.h>

/*
 * Standard items length
 */

#define BUFLEN				4096				/* Buffer */
#define CODELEN				32					/* Item */
#define DATELEN				32					/* Date */
#define GRANTLEN			32					/* Grant */
#define IDLEN					128					/* Client_Id */
#define ITEMLEN				255					/* Item */
#define PASSWORDLEN		64					/* Password */
#define TOKENLEN			32					/* Token */
#define URILEN				2048				/* URI */


/*
 * String generator
 */

#define MKSTR_PASSWORD	0
#define MKSTR_SALT			1
#define MKSTR_ID				2
#define MKSTR_GRANT			3
#define MKSTR_TOKEN			4

extern int mkstr(char *, size_t, int);

/*
 * The registered clients file:
 *
 * <client_id>:<client_secret>:confidential|public:<auth>:
 * 	<grant_validity>:<token_validity>:<refresh token validity>:
 * 	<redirect_uri>,...
 *
 * <grant_validity>: grant ticket validity (for code authentication, 
 * 		default is 10 minutes).
 * <token_validity>: token validity (0 means forever, default is 
 * 		1 hour for implicit, 10 hours for others).
 * <refresh_validity>: token validity (0 means forever,
 * 	 -1 means no refresh token, default is 10 days).
 */

#define OAUTH2_CLIENTS OAUTH2DIR "/clients"

#define NO_AUTH					0
#define CODE_AUTH				1
#define IMPLICIT_AUTH		2
#define PASSWORD_AUTH		3
#define CLIENT_AUTH			4

#define NO_GRANT					0
#define CODE_GRANT				1
#define IMPLICIT_GRANT		2
#define PASSWORD_GRANT		3
#define CLIENT_GRANT			4

#define NURIS	(15+1)

struct oa2_client {
	char buf[16192];
	int confidential, auth;
	long grant_validity, token_validity, refresh_token_validity;
	const char *secret; 
	const char *redirect_uri[NURIS];
};

extern int oa2_client_find(const char *, struct oa2_client *);
extern int oa2_client_store(const char *,
									const char *, int, int,
									long, long, long,
									char * const *); 

/*
 * The grant file:
 *
 * <grant>:<validity>:<status>:<client_id>:<user_id>:<scope>:<state>:
 * 	<redirect_uri>
 *
 * <validity> is the date where the grant becomes invalid (0 means forever
 * valid).
 * <status> is wait_code, wait_implicit, granted.
 *
 */

#define OAUTH2_GRANTS OAUTH2DIR "/grants"

#define WAIT_CODE_STATUS			1
#define WAIT_IMPLICIT_STATUS	2
#define GRANTED_STATUS				3

struct oa2_grant {
	char buf[8192];
	time_t validity;
	int status;
	const char *client_id, *user_id, *scope, *state, *redirect_uri; 
};

extern int oa2_grant_find(const char *, struct oa2_grant *);
extern int oa2_grant_store(const char *, time_t, int, const char *,
								const char *, const char *, const char *);
extern void oa2_grant_update(const char *, time_t, int, const char *);
extern void oa2_grant_delete(const char *);
extern void oa2_grant_expire(void);

/*
 * The token file:
 *
 * <token>:<validity>:normal|refresh:<client_id>:<user_id>:<scope>
 *
 * <validity> is the date where the tokens becomes invalid (0 means forever
 * valid, and < 0 are special token not usable now).
 */

#define OAUTH2_TOKENS OAUTH2DIR "/tokens"

struct oa2_token {
	char buf[8192];
	int refresh;
	const char *token, *client_id, *user_id, *scope; 
	time_t validity;
};

extern int oa2_token_find(const char *, struct oa2_token *);
extern int oa2_token_store(const char *, time_t, int,
								const char *, const char *, const char *);
extern void oa2_token_update(const char *, time_t);
extern void oa2_token_delete(const char *);
extern void oa2_token_expire(void);

/*
 * URI Encoding/Decoding
 */

extern int oa2_check_uri(const char *);
extern char *oa2_encode(const char *),
			 		*oa2_decode(const char *);

#endif
