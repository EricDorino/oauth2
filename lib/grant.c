/*
 * Copyright (C) 2016  Éric Dorino.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <sys/file.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include "oauth2d.h"

int oa2_grant_find(const char *grant, struct oa2_grant *gr)
{
	FILE *f;
	char *buf, *p, *validity, *status, *scope, *state, *uri;

	if (!(f = fopen(OAUTH2_GRANTS, "r")))
		return 0;

	if (flock(fileno(f), LOCK_EX) < 0)
		return 0;

	while (fgets(gr->buf, sizeof(gr->buf), f)) {
		buf = gr->buf;
		if (!(p = strsep(&buf, ":"))) {
			errno = EINVAL;
			goto not_found;
		}
		if (strcmp(grant, p) != 0)
			continue;
		errno = 0;

		validity = strsep(&buf, ":");
		gr->validity = (time_t)atol(validity);
		status = strsep(&buf, ":");
		gr->status = strcmp(status, "wait_code") == 0 ? WAIT_CODE_STATUS :
								 strcmp(status, "wait_implicit") == 0 ? WAIT_IMPLICIT_STATUS :
								 strcmp(status, "granted") == 0 ? GRANTED_STATUS : 0;
		gr->client_id = strsep(&buf, ":");
		gr->user_id = strsep(&buf, ":");
		scope = strsep(&buf, ":");
		state = strsep(&buf, ":");
		uri = strsep(&buf, "\n");

		if (strlen(scope) == 0)
			gr->scope = NULL;
		else {
			char *scope0 = oa2_decode(scope);

			strcpy(scope, scope0);
			free(scope0);
			gr->scope = scope;
		}
		if (strlen(state) == 0)
			gr->state = NULL;
		else {
			char *state0 = oa2_decode(state);

			strcpy(state, state0);
			free(state0);
			gr->state = state;
		}
		if (strlen(uri) == 0)
			gr->redirect_uri = NULL;
		else {
			char *uri0 = oa2_decode(uri);

			strcpy(uri, uri0);
			free(uri0);
			gr->redirect_uri = uri;
		}
		flock(fileno(f), LOCK_UN);
		fclose(f);
		return 1;
	}

	not_found:
	flock(fileno(f), LOCK_UN);
	fclose(f);
	return 0;
}

int oa2_grant_store(const char *grant,
							time_t validity,
							int status,
							const char *client_id,
							const char* scope,
							const char *state, 
							const char *redirect_uri)
{
	FILE *f;
	char *sc = NULL, *st = NULL, *uri = NULL;

	if (!(f = fopen(OAUTH2_GRANTS, "a")))
		return 0;

	if (flock(fileno(f), LOCK_EX) < 0)
		return 0;

	if (scope)
		sc = oa2_encode(scope);
	if (state)
		st = oa2_encode(state); 
	if (redirect_uri)
		uri = oa2_encode(redirect_uri);

	fprintf(f, "%s:%ld:%s:%s::%s:%s:%s\n",
			grant, (long)validity,
			status == WAIT_CODE_STATUS ? "wait_code" : 
				status == WAIT_IMPLICIT_STATUS ? "wait_implicit" :
				status == GRANTED_STATUS ? "granted" : 0,
			client_id ? : "",
			sc ? sc : "",
			st ? st : "",
			uri ?  uri : "");

	free(sc);
	free(st);
	free(uri);

	flock(fileno(f), LOCK_UN);
	fclose(f);
	return 1;
}	

void oa2_grant_delete(const char *grant)
{
	FILE *oldf, *newf;
	char buf[8192], *p;

	if (!(oldf = fopen(OAUTH2_GRANTS, "r")))
		return;
	if (!(newf = fopen(OAUTH2_GRANTS ".new", "w")))
		return;

	if (flock(fileno(oldf), LOCK_EX) < 0)
		return;

	while (fgets(buf, sizeof(buf), oldf)) {
		p = index(buf, ':');
		if (p && strncmp(grant, buf, p-buf) == 0) {
			continue;
		}
		fputs(buf, newf);
	}
	flock(fileno(oldf), LOCK_UN);
	fclose(oldf);
	fclose(newf);
	rename(OAUTH2_GRANTS ".new", OAUTH2_GRANTS);
}

void oa2_grant_update(const char *grant, time_t validity,
								int status, const char *user_id)
{
	FILE *oldf, *newf;
	char buf[8192], *p, *q;;

	if (!(oldf = fopen(OAUTH2_GRANTS, "r"))) {
		return;
	}

	if (flock(fileno(oldf), LOCK_EX) < 0)
		return;

	if (!(newf = fopen(OAUTH2_GRANTS".new", "w"))) {
		return;
	}

	while (fgets(buf, sizeof(buf), oldf)) {
		p = index(buf, ':');
		if (p && strncmp(grant, buf, p-buf) == 0) {
			char *notused0, *notused1, *client_id, *notused2,
					 *scope, *state, *redirect_uri;

			p++;
			notused0 = strsep(&p, ":");	/* validity */
			notused1 = strsep(&p, ":");	/* status */
			client_id = strsep(&p, ":");
			notused2 = strsep(&p, ":");	/* user_id */
			scope = strsep(&p, ":");
			state = strsep(&p, ":");
			redirect_uri = strsep(&p, "\n");
			fprintf(newf, "%s:%ld:%s:%s:%s:%s:%s:%s\n",
				grant, (long)validity,
				status == WAIT_CODE_STATUS ? "wait_code" : 
				status == WAIT_IMPLICIT_STATUS ? "wait_implicit" :
				status == GRANTED_STATUS ? "granted" : 0,
				client_id,
				user_id,
				scope, state,
				redirect_uri);
			continue;
		}
		fputs(buf, newf);
	}
	flock(fileno(oldf), LOCK_UN);
	fclose(oldf);
	fclose(newf);
	rename(OAUTH2_GRANTS".new", OAUTH2_GRANTS);
}

void oa2_grant_expire(void)
{
	FILE *oldf, *newf;
	char buf[8192], *p, *q;
	time_t now = time(NULL);

	if (!(oldf = fopen(OAUTH2_GRANTS, "r")))
		return;
	if (!(newf = fopen(OAUTH2_GRANTS ".new", "w")))
		return;

	if (flock(fileno(oldf), LOCK_EX) < 0)
		return;

	while (fgets(buf, sizeof(buf), oldf)) {
		p = index(buf, ':');
		if (p && (q = index(p+1, ':'))) {
			char val[32];
			time_t validity;

			memset(val, 0, sizeof(val));
			strncpy(val, p+1, q-p);
			validity = atol(val);
			if (validity > 0 && validity < now)
				continue;
		}
		fputs(buf, newf);
	}
	flock(fileno(oldf), LOCK_UN);
	fclose(oldf);
	fclose(newf);
	rename(OAUTH2_GRANTS ".new", OAUTH2_GRANTS);
}
