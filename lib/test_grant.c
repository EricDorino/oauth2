/*
 * Copyright (C) 2016  Éric Dorino.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include "oauth2d.h"

static const char *status(int s) {
	static char buf[20];
	switch (s) {
		case WAIT_CODE_STATUS: return "wait_code";
		case WAIT_IMPLICIT_STATUS: return "wait_implicit";
		case GRANTED_STATUS: return "granted";
		default:
			sprintf(buf, "UNKNOWN(%d)", s);
			return &buf[0];
	}
}

static int find(const char *grant)
{
	struct oa2_grant g;

	if (oa2_grant_find(grant, &g)) {
		printf("validity: %ld\nstatus: %s\nclient_id: \"%s\"\nuser_id: \"%s\"\n"
					 "scope: \"%s\"\nstate: \"%s\"\nredirect_uri: \"%s\"\n\n",
					 g.validity, status(g.status), g.client_id, g.user_id, g.scope,
					 g.state, g.redirect_uri);
		return 1;
	}
	else if (errno > 0)
			perror("test_grant");
	else
			printf("Not found %s\n\n", grant);
	fprintf(stderr, "%s\n", grant);
	return 0;
}

int main(void)
{
	if (!oa2_grant_store("GRANT0", time(NULL), WAIT_CODE_STATUS,
					"client_id0", NULL, "State", "http://GRANT0")) {
		perror("test_grant");
		return EXIT_FAILURE;
	}
	oa2_grant_store("GRANT1", time(NULL)+15000, WAIT_IMPLICIT_STATUS,
					"client_id0", "feeds", NULL, NULL);
	oa2_grant_store("GRANT2", 0, WAIT_CODE_STATUS,
					"client_id0", "feeds calendar", "State", NULL);
	oa2_grant_store("GRANT3", -1, WAIT_CODE_STATUS,
					"client_id0", NULL, NULL, "http://GRANT3");

	find("INEXISTANT");
	find("GRANT0");
	find("GRANT1");
	find("GRANT2");
	find("GRANT3");

	printf("delete INEXISTANT\n");
	oa2_grant_delete("INEXISTANT");
	printf("delete GRANT2\n");
	oa2_grant_delete("GRANT2");
	find("GRANT2");

	sleep(5);
	printf("expire %lu\n", time(NULL));
	oa2_grant_expire();
	find("GRANT0");
	find("GRANT1");
	find("GRANT3");

	oa2_grant_update("GRANT3", time(NULL)+10000, GRANTED_STATUS, "user_id0");
	find("GRANT3");

	return EXIT_SUCCESS;
}
