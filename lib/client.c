/*
 * Copyright (C) 2016  Éric Dorino.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <sys/file.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "oauth2d.h"

int oa2_client_find(const char *id, struct oa2_client *cl)
{
	FILE *f;
	char *buf, *p1, *p, *secret, *confidential,
			 *grant_val, *token_val, *rtoken_val, *auth, *uris;
	int i;

	if (!(f = fopen(OAUTH2_CLIENTS, "r")))
		return 0;

	if (flock(fileno(f), LOCK_EX) < 0)
		return 0;

	while (fgets(cl->buf, sizeof(cl->buf), f)) {
		buf = cl->buf;
		p = strsep(&buf, ":");
		if (!p)
			goto not_found;
		if (strcmp(id, p) != 0)
			continue;
		errno = 0;

		secret = strsep(&buf, ":");
		if (strlen(secret) > 0)
			cl->secret = secret;
		else
			cl->secret = NULL;
		confidential = strsep(&buf, ":");
		auth = strsep(&buf, ":");
		grant_val = strsep(&buf, ":");
		token_val = strsep(&buf, ":");
		rtoken_val = strsep(&buf, ":");
		uris = strsep(&buf, "\n");

		if (strncmp(confidential, "confidential", 12) == 0)
			cl->confidential = 1;
		else
			cl->confidential = 0;

		if (strcmp(auth, "code") == 0)
			cl->auth = CODE_AUTH;
		else if (strcmp(auth, "implicit") == 0)
			cl->auth = IMPLICIT_AUTH;
		else if (strcmp(auth, "password") == 0)
			cl->auth = PASSWORD_AUTH;
		else if (strcmp(auth, "client") == 0)
			cl->auth = CLIENT_AUTH;
		else
			cl->auth = CODE_AUTH;

		cl->grant_validity = atol(grant_val);
		cl->token_validity = atol(token_val);
		cl->refresh_token_validity = atol(rtoken_val);

		memset(cl->redirect_uri, 0, NURIS*sizeof(*cl->redirect_uri));
		p1 = strsep(&uris, ",");
		i = 0;
		while (p1 && strlen(p1) > 0 && i < NURIS) {
			char *p2 = oa2_decode(p1);

			strcpy(p1, p2);
			cl->redirect_uri[i++] = p1;
			free(p2);
			p1 = strsep(&uris, ",:");
		}
		flock(fileno(f), LOCK_UN);
		fclose(f);
		return 1;
	}

	not_found:
	errno = EINVAL;
	flock(fileno(f), LOCK_UN);
	fclose(f);
	return 0;
}

int oa2_client_store(const char *id,
								const char *secret, 
								int confidential,
							 	int auth, 
								long grant_validity,
								long token_validity,
								long refresh_token_validity,
								char * const *uri)
{
	FILE *f;

	if (!(f = fopen(OAUTH2_CLIENTS, "a")))
		return 0;

	if (flock(fileno(f), LOCK_EX) < 0)
		return 0;

	fprintf(f, "%s:%s:%s:", id, secret, confidential?"confidential":"public");

	if (auth == CODE_AUTH)
		fputs("code", f);
	else if (auth == IMPLICIT_AUTH)
		fputs("implicit", f);
	else if (auth == PASSWORD_AUTH)
		fputs("password", f);
	else if (auth == CLIENT_AUTH)
		fputs("client", f);
	else
		fputs("code", f);

	fprintf(f, ":%ld:%ld:%ld:", 
			grant_validity, token_validity, refresh_token_validity);

	while (*uri) {
		fputs(*uri, f);
		*uri++;
		if (*uri)
			fputc(',', f);
	}
	fputc('\n', f);

	flock(fileno(f), LOCK_UN);
	fclose(f);
	return 1;
}	
