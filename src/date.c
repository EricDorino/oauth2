/*
 * Copyright (C) 2016,  Éric Dorino.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include "global.h"

/*
 * Format date with offset seconds.
 */

char *date(char *buf, size_t size, int offset)
{
	time_t t = time(NULL) + offset;
	struct tm *tm = gmtime(&t);

	strftime(buf, size, "%a, %d %b %Y %T %Z", tm);
	return buf;
}
