/*
 * Copyright (C) 2016  Éric Dorino.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <httpserver.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <wait.h>
#include "check_user.h"
#include "global.h"
#include "oauth2d.h"

/*
 * Authenticate a local user.
 */

int check_user(struct http_connection *conn,
								const char *username,
								const char *password)
{
	pid_t pid;
	int in[2], status;
	FILE *fin;

	if (pipe(in) < 0)
		return 0;
	if ((pid = fork()) < 0)
		return 0;
	else if (pid == 0) {
		if (dup2(in[0], STDIN_FILENO) < 0)
			return 0;
		execl(http_get_config(conn)->auth_program,
					http_get_config(conn)->auth_program, NULL);
		return 0;
	}
	close(in[0]);
	if (!(fin = fdopen(in[1], "w")))
		return 0;
	fprintf(fin, "%s\n%s\n", username, password);
	fclose(fin);
	wait(&status);
	return WIFEXITED(status) ? WEXITSTATUS(status) : 50;
}

const char *check_user_status(int status)
{
	switch (status) {
		case STATUS_OK:
			return "OK";
		case STATUS_UNKNOWN:
		case STATUS_INVALID:
			return "Invalid user/password";
		case STATUS_BLOCKED:
			return "Blocked user";
		case STATUS_EXPIRED:
			return "Expired user";
		case STATUS_PW_EXPIRED:
			return "Expired password";
		case STATUS_NOLOGIN:
			return "Login denied";
		case STATUS_MANYFAILS:
			return "Too many login fails";
		default:
			if (status >= STATUS_MISCONFIGURED)
				return "Configuration error";
			else return "Unknown";
	}
}
