/*
 * Copyright (C) 2016  Éric Dorino.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define _GNU_SOURCE
#include <crypt.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <syslog.h>
#include "global.h"

/*
 * A scope is a string of the form
 * 
 * scope := application* | empty
 * application := name
 *
 */

int is_valid_scope(const char *scope)
{
	char *s, *p;
	int ret = 0;

	if (!scope || strlen(scope) == 0)
		return 1;

	if (!(s = strdup(scope)))
		return 0;

#if 0
	while ((p = strsep(&s, " ")) != NULL) {
		/* FIXME Something to check */
	}
#else
	ret = 1;
#endif

	free(s);
	return ret;
}

int enforce_scope(const char *token_scope, const char *scope)
{
	char *p, *q, *r, *s;
	int ret = 1;

	if (!is_valid_scope(scope))
		return 0;

	if (!(token_scope && strlen(token_scope) > 0))
		return 1;

	if (!(s = strdup(scope)))
		return 0;

	while ((p = strsep(&s, " ")) != NULL)
		if (!strstr(token_scope, p))
			ret = 0;

	free(s);
	return ret;
}

