/*
 * Copyright (C) 2016 Éric Dorino.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef check_user_h_
#define check_user_h_
#include <httpserver.h>

/*
 * pwauth(1) return codes.
 */

#define	STATUS_OK							0
#define	STATUS_UNKNOWN				1
#define	STATUS_INVALID				2
#define	STATUS_BLOCKED				3
#define	STATUS_EXPIRED				4
#define	STATUS_PW_EXPIRED			5
#define	STATUS_NOLOGIN				6
#define	STATUS_MANYFAILS			7
#define	STATUS_MISCONFIGURED	50

extern int check_user(struct http_connection *, const char *, const char *);
extern const char *check_user_status(int);

#endif
