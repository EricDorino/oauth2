/*
 * Copyright (C) 2016  Éric Dorino.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define _GNU_SOURCE
#include <crypt.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "check_user.h"
#include "global.h"

/*
 * RFC 6749 - OAuth 2.0
 *
 * Authorization code grant and implicit authorization
 */

/*
 * Send a direct response to a requester, in case of no valid redirect_uri.
 */

static void send_response(struct http_connection *conn,
								int status,
								const char *text,
								const char *reason,
								const char *description)
{
	char dbuf[DATELEN];

	http_trace(conn, "send_response \"%s\"", reason);

	http_set_status_code(conn, status);
	http_printf(conn, "HTTP/1.1 %d %s\r\nServer: %s\r\nDate: %s\r\n",
		status, text,
		http_get_config(conn)->server_software,
		date(dbuf, sizeof(dbuf), 0));
	if (http_get_header(conn, "Origin"))
		http_printf(conn,	"Access-Control-Allow-Origin: *\r\n");
	http_printf(conn,
		"Content-Type: application/json; charset=\"utf-8\"\r\n\r\n"
		"{\r\n \"error\":\"%s\"",
		reason);
	if (description && strlen(description))
		http_printf(conn, ",\r\n\"error_description\":\"%s\"", description);
	http_printf(conn, "\r\n}\r\n");
}

/*
 * Try to send a 302 redirection, with an error value;
 * send directly if no redirect_uri is supplied.
 */

static void send_redirected_response(struct http_connection *conn,
								const char *redirect_uri,
								const char *reason,
								const char *description,
								const char *state)
{
	char dbuf[DATELEN];

	http_trace(conn, "send_redirected_response [%s] \"%s\" \"%s\"", 
									redirect_uri, reason, description);

	if (redirect_uri && strlen(redirect_uri) > 0)	{
		http_set_status_code(conn, 302);
		http_printf(conn, "HTTP/1.1 302 Found\r\nLocation: %s?error=%s",
			redirect_uri, reason);
		if (description && strlen(description) > 0)
			http_printf(conn, "&error_description=%s", description);
		if (state && strlen(state) > 0)
			http_printf(conn, "&state=%s", state);
		http_printf(conn, "\r\nServer: %s\r\nDate: %s\r\n",
			http_get_config(conn)->server_software,
			date(dbuf, sizeof(dbuf), 0));
		if (http_get_header(conn, "Origin"))
			http_printf(conn,	"Access-Control-Allow-Origin: *\r\n");
		http_write(conn, "\r\n", 2);
	}
	else
		send_response(conn, 400, "Bad Request", reason, description);
}

/*
 * Standard (valid) redirection response.
 */

static void send_location(struct http_connection *conn,
									const char *location,...)
{
	va_list args;
	char dbuf[DATELEN];

	http_trace(conn, "send_location [%s]", location);

	va_start(args, location);

	http_set_status_code(conn, 302);
	http_printf(conn, "HTTP/1.1 302 Found\r\nLocation: ");
	http_vprintf(conn, location, args);
	http_printf(conn,	"\r\nServer: %s\r\nDate: %s\r\n",
			http_get_config(conn)->server_software,
			date(dbuf, sizeof(dbuf), 0));
	if (http_get_header(conn, "Origin"))
		http_printf(conn,	"Access-Control-Allow-Origin: *\r\n");
	http_write(conn, "\r\n", 2);
	va_end(args);
}

/*
 * Authorization code grant and implicit authorization handling.
 * Redirect to the Consent form.
 */

static void handle_grant(struct http_connection *conn, const char *buf)
{
	char response_type[CODELEN],
			 client_id[ITEMLEN], 
			 redirect_uri[URILEN], 
			 scope[ITEMLEN], 
			 state[ITEMLEN],
			 grant[GRANTLEN];
	struct oa2_client c;
	time_t now = time(NULL);

	http_trace(conn, "handle_grant [%s]", buf);

	http_get_var(buf, strlen(buf), "redirect_uri",
							redirect_uri, sizeof(redirect_uri));
	http_get_var(buf, strlen(buf), "state", state, sizeof(state));
	http_get_var(buf, strlen(buf), "scope", scope, sizeof(scope));

	if (!(http_get_var(buf, strlen(buf), "response_type",
			  					 response_type, sizeof(response_type)) > 0 &&
				(strcmp(response_type, "code") == 0 ||
			 	 strcmp(response_type, "token") == 0))) {
		send_redirected_response(conn, redirect_uri,
										"unsupported_response_type", NULL, state);
		return;
	}
	if (!(http_get_var(buf, strlen(buf), "client_id",
									 client_id, sizeof(client_id)) > 0 &&
				oa2_client_find(client_id, &c))) { 
		send_redirected_response(conn, redirect_uri,
										"invalid_client", NULL, state);
		return;
	}
	if (!((strcmp(response_type, "code") == 0 && (c.auth == CODE_AUTH)) ||
			(strcmp(response_type, "token") == 0 && (c.auth == IMPLICIT_AUTH)))) {
		send_redirected_response(conn, redirect_uri,
						"unauthorized_client", NULL, state);
		return;
	}
	if (!is_valid_scope(scope)) {
		send_redirected_response(conn, redirect_uri, "invalid_scope", NULL, state);
		return;
	}
	/*
	 * Use the supplied redirection URI, if any, and if it match a
	 * registered one.
	 * If none is supplied, use the first registered one.
	 */
	if (strlen(redirect_uri) > 0) {
		int i = 0, found = 0;

		while (c.redirect_uri[i] && !found) {
			if (strcmp(c.redirect_uri[i], redirect_uri) == 0)
				found = 1;
			else 
				i++;
		}
		if (!found) {
			send_redirected_response(conn, redirect_uri, "invalid_grant", 
								"The supplied URL does not match a registered URL", state);
			return;
		}
	}
	else if (!c.redirect_uri[0]) {
		send_redirected_response(conn, redirect_uri, "invalid_request",
							"No supplied URL, and no registered URL", state);
		return;
	}
	else
		strcpy(redirect_uri, c.redirect_uri[0]);

	/*
	 * While waiting for resource owner consent.
	 */

	if (!(mkstr(grant, sizeof(grant), MKSTR_GRANT) &&
		  	oa2_grant_store(grant, now+c.grant_validity,
								strcmp(response_type, "code") == 0 ?
									WAIT_CODE_STATUS : 
									WAIT_IMPLICIT_STATUS,
								client_id, scope, state, redirect_uri))) {
			send_redirected_response(conn, redirect_uri,
										"server_error", "Cannot store grant", state);
			return;
		}
	/* 
	 * Send the consent form to the resource owner 
	 */
	send_location(conn, "%s?grant=%s", consent_form, grant);
}

/*
 * Resource Owner consent to grant.
 * Check credentials and inform requester.
 */

int oauth2_consent_accept(struct http_connection *conn)
{
	struct http_request *req = http_get_request(conn);
	char buf[BUFLEN] = "",
			 username[ITEMLEN],
			 password[ITEMLEN],
			 grant[GRANTLEN];
	struct oa2_grant g;
	int status;
	time_t now = time(NULL);

	http_trace(conn, "oauth2_consent_accept");

	g.redirect_uri = g.state = NULL;

	if (!(strcmp(req->method, "POST") == 0 && 
				http_read(conn, buf, sizeof(buf)) > 0 &&
				http_get_var(buf, strlen(buf), "grant", grant, sizeof(grant)) > 0 &&
				oa2_grant_find(grant, &g) &&
				g.status == WAIT_CODE_STATUS &&
				now < g.validity &&
				http_get_var(buf, strlen(buf), "username",
							username, sizeof(username)) > 0 &&
				http_get_var(buf, strlen(buf), "password",
							password, sizeof(password)) > 0)) {
		send_redirected_response(conn, g.redirect_uri, "invalid_request",
										NULL, g.state);
		return 1;
	}
	if (g.validity < now) {
		send_redirected_response(conn, g.redirect_uri, "access_denied",
											"grant_expired", g.state); /*FIXME*/
		return 1;
	}
	if ((status = check_user(conn, username, password)) != STATUS_OK) {
		if (status == STATUS_UNKNOWN || status == STATUS_INVALID) /* Retry... */
			send_location(conn, "%s?grant=%s", consent_form, grant);
		else if (status != STATUS_OK)
			send_redirected_response(conn, g.redirect_uri, "access_denied",
											check_user_status(status), g.state);
		return 1;
	}
	if (g.status == WAIT_CODE_STATUS) {
		/*
		 * Deliver a grant (Authorization code grant).
		 */
		oa2_grant_update(grant, g.validity, GRANTED_STATUS, username);
		send_location(conn, "%s?code=%s%s%s",
				g.redirect_uri,
				grant,
				g.state ? "&state=" : "", g.state ? g.state : "");
	}
	else if (g.status == WAIT_IMPLICIT_STATUS) {
		/*
		 * Deliver a token (Implicit authorization)
		 */
		struct oa2_client c;

		if (!oa2_client_find(g.client_id, &c))
			send_redirected_response(conn, g.redirect_uri,
							"server_error", NULL, g.state);
		else {
			oa2_grant_delete(grant);
			oa2_token_store(grant, now+c.token_validity, 0,
										g.client_id, username, g.scope);
			send_location(conn,
				"%s#access_token=%s&token_type=Bearer&expires_in=%ld%s%s%s%s",
				g.redirect_uri, grant, c.token_validity,
				g.scope ? "&scope=" : "", g.scope ? g.scope : "",
				g.state ? "&state=" : "", g.state ? g.state : "");
		}
	}
	else
		send_redirected_response(conn, g.redirect_uri, "access_denied",
											"invalid_grant", g.state); /*FIXME*/
	return 1;
}

/*
 * Resource Owner has denied.
 */

int oauth2_consent_deny(struct http_connection *conn)
{
	struct http_request *req = http_get_request(conn);
	char buf[BUFLEN] = "",
			 grant[TOKENLEN];
	struct oa2_grant g;

	http_trace(conn, "oauth2_consent_deny");

	g.redirect_uri = g.state = NULL;

	if (!(strcmp(req->method, "POST") == 0 && 
				http_read(conn, buf, sizeof(buf)) > 0 &&
				http_get_var(buf, strlen(buf), "grant", grant, sizeof(grant)) > 0 &&
				oa2_grant_find(grant, &g))) {
		send_redirected_response(conn, g.redirect_uri, "invalid_request",
										NULL, g.state);
		return 1;
	}
	send_location(conn, "%s?error=access_denied%s%s",
									g.redirect_uri,
									g.state ? "&state=" : "",
									g.state ? g.state : "");
	oa2_grant_delete(grant);
	return 1;
}

int oauth2_grant(struct http_connection *conn)
{
	struct http_request *req = http_get_request(conn);
	char buf[BUFLEN] = "";

	if (strcmp(req->method, "GET") == 0 && req->query_string) 
			handle_grant(conn, req->query_string);
	else if (strcmp(req->method, "POST") == 0 && 
					 http_read(conn, buf, sizeof(buf)) > 0)
			handle_grant(conn, buf);
	else
		http_send_response(conn, 400, "Bad Request");
	return 1;
}
