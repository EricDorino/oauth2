/*
 * Copyright (C) 2016  Éric Dorino.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <ctype.h>
#include <httpserver.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include "global.h"

extern int oauth2_grant(struct http_connection *);
extern int oauth2_token(struct http_connection *);
#ifdef TOKENINFO
extern int oauth2_tokeninfo(struct http_connection *);
#endif
extern int oauth2_consent_accept(struct http_connection *);
extern int oauth2_consent_deny(struct http_connection *);

/*
 * httpserver callbacks.
 */

int oauth2_begin_request(struct http_connection *conn)
{
	struct http_request *rq = http_get_request(conn);

	http_trace(conn, "begin_request %s %s", rq->method, rq->uri);

	if (strncmp(rq->uri, "/oauth2/authorize", 17) == 0)
		return oauth2_grant(conn);
	else if (strncmp(rq->uri, "/oauth2/grant", 13) == 0)
		return oauth2_grant(conn);
#ifdef TOKENINFO
	else if (strncmp(rq->uri, "/oauth2/tokeninfo", 17) == 0)
		return oauth2_tokeninfo(conn);
#endif
	else if (strncmp(rq->uri, "/oauth2/token", 13) == 0)
		return oauth2_token(conn);
	else if (strncmp(rq->uri, "/oauth2/consent_accept", 22) == 0)
		return oauth2_consent_accept(conn);
	else if (strncmp(rq->uri, "/oauth2/consent_deny", 20) == 0)
		return oauth2_consent_deny(conn);
	else {
		http_trace(conn, "back to server");
		return 0; /* Let the server handle that request */
	}
}

int oauth2_http_response(struct http_connection *conn, 
						int status, const char *status_text, va_list args)
{
	int close_connection = (status == 401 || status == 404 || status >= 500);
	char dbuf[64];

	http_trace(conn, "http_response %d %s", status, status_text);

	http_set_status_code(conn, status);

	http_printf(conn, "HTTP/1.1 %d ", status);
 	http_vprintf(conn, status_text, args);
	http_printf(conn, "\r\nServer: %s\r\nDate: %s\r\nConnection: %s\r\n\r\n",
		http_get_config(conn)->server_software,
		date(dbuf, sizeof(dbuf), 0),
		close_connection ? "close" : "keep-alive");	
	if (close_connection)
		http_must_close(conn);
	return 1;
}
