/*
 * Copyright (C) 2016  Éric Dorino.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define _GNU_SOURCE
#include <crypt.h>
#include <httpserver.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <wait.h>
#include "oauth2d.h"
#include "check_user.h"
#include "global.h"

/*
 * RFC 6749 - OAuth 2.0
 */

/*
 * Get client_id/client_secret credentials, either in a Basic header, or
 * in the post fields.
 * The client_secret may be omitted, in case the client is public.
 */

static int get_client_credentials(struct http_connection *conn,
												 const char *buf,
												 char *client_id, size_t client_id_size,
												 char *client_secret, size_t client_secret_size)
{
	const char *password;

	if (!(password = http_basic_auth_password(conn))) {
		/* No Basic auth, get http params instead */
		if (http_get_var(buf, strlen(buf), "client_id",
										 client_id, client_id_size) < 0) 
			return 0;
		else {
			http_get_var(buf, strlen(buf), "client_secret",
										 client_secret, client_secret_size);
			return 1;
		}
	}
	else {
		strncpy(client_id, http_get_request(conn)->remote_user, client_id_size);
		strncpy(client_secret, password, client_secret_size);
		free((char *)password);
		free((char *)http_get_request(conn)->remote_user);
		http_get_request(conn)->remote_user = NULL;
		return 1;
	}
}

/*
 * Check client credentials.
 */

static int enforce_client_credentials(struct http_connection *conn,
								const char *client_id,
								const char *client_secret,
								const char *c_client_secret)
{
	struct crypt_data data;
	char *p;

	data.initialized = 0;
	if (client_secret &&
			c_client_secret &&
			(p = crypt_r(client_secret, c_client_secret, &data)) &&
			strcmp(p, c_client_secret) == 0)
		return 1;
	else
		return 0;
}

/*
 * The client credentials check failed.
 */

static void send_response_401(struct http_connection *conn)
{
	char buf[DATELEN];

	http_trace(conn, "send_response_401");

	http_set_status_code(conn, 401);
	http_printf(conn,
			"HTTP/1.1 401 Unauthorized\r\n"
			"Server: %s\r\nDate: %s\r\n"
			"WWW-Authenticate: Basic realm=\"%s\"\r\n",
			http_get_config(conn)->server_software,
			date(buf, sizeof(buf), 0),
			http_get_config(conn)->domain);
	if (http_get_header(conn, "Origin"))
		http_printf(conn, "Access-Control-Allow-Origin: *\r\n");
	http_write(conn, "\r\n", 2);
}

/*
 * Everything is fine. Send requested tokens.
 */

static void send_response_200(struct http_connection *conn,
								const char *token, long expires_in,
								const char *refresh_token,
								const char *scope)
{
	char buf[DATELEN];

	http_trace(conn, "send_response_200");

	http_set_status_code(conn, 200);
	http_printf(conn,
			"HTTP/1.1 200 OK\r\n"
			"Server: %s\r\nDate: %s\r\n"
			"Content-Type: application/json; charset=UTF-8\r\n"
			"Cache-Control: no-store\r\nPragma: no-cache\r\n",
			http_get_config(conn)->server_software,
			date(buf, sizeof(buf), 0));
	if (http_get_header(conn, "Origin"))
		http_printf(conn, "Access-Control-Allow-Origin: *\r\n");
	http_write(conn, "\r\n", 2);
	http_printf(conn,
			"{\r\n"
			" \"access_token\":\"%s\",\r\n"
			" \"token_type\":\"Bearer\",\r\n"
			" \"expires_in\":%ld",
			token,
			3600);
	if (refresh_token && strlen(refresh_token) > 0) 
		http_printf(conn, ",\r\n \"refresh_token\":\"%s\"", refresh_token);
	if (scope && strlen(scope) > 0) 
		http_printf(conn, ",\r\n \"scope\":\"%s\"", scope);
	http_printf(conn, "\r\n}\r\n");
}

/*
 * Something goes wrong.
 */

static void send_response(struct http_connection *conn,
								int status, const char *text,
								const char *reason,
								const char *description)
{
	char buf[DATELEN];

	http_trace(conn, "send_response %d %s \"%s\" \"%s\"",
									status, text, reason, description);

	http_set_status_code(conn, status);
	http_printf(conn,
		"HTTP/1.1 %d %s\r\n"
		"Server: %s\r\nDate: %s\r\n"
		"Content-Type: application/json; charset=UTF-8\r\n"
		"Cache-Control: no-store\r\nPragma: no-cache\r\n",
		status, text,
		http_get_config(conn)->server_software,
		date(buf, sizeof(buf), 0));
	if (http_get_header(conn, "Origin"))
		http_printf(conn, "Access-Control-Allow-Origin: *\r\n");
	http_write(conn, "\r\n", 2);
	http_printf(conn,
		"{\r\n \"error\":\"%s\"",
		reason);
	if (description && strlen(description) > 0)
		http_printf(conn, ",\r\n \"error_description\":\"%s\"", description);
	http_printf(conn, "\r\n}\r\n");
}

/*
 * Authorization code grant
 */

static void handle_code_token(struct http_connection *conn, const char *buf)
{
	char code[CODELEN], 
			 client_id[IDLEN], 
			 client_secret[PASSWORDLEN],
			 redirect_uri[URILEN],	
			 token[TOKENLEN],
			 refresh_token[TOKENLEN];
	struct oa2_client c;
	struct oa2_grant g;
	time_t now = time(NULL);

	http_trace(conn, "handle_code_token [%s]", buf);

	if (!(http_get_var(buf, strlen(buf), "code", code, sizeof(code)) > 0 &&
				get_client_credentials(conn, buf, 
												client_id, sizeof(client_id),
												client_secret, sizeof(client_secret)))) {
		send_response(conn, 400, "Bad Request", "invalid_request", NULL);
		return;
	}
	if (!oa2_client_find(client_id, &c)) {
		send_response(conn, 400, "Bad Request", "invalid_client", NULL);
		return;
	}
	if (c.auth != CODE_AUTH) {
		send_response(conn, 400, "Bad Request", "unauthorized_client", NULL);
		return;
	}
	if ((c.confidential || c.secret) &&
			!(enforce_client_credentials(conn, client_id,
											client_secret, c.secret))) {
		send_response_401(conn);
		return;
	}
	memset(client_secret, 0, sizeof(client_secret));
	http_get_var(buf, strlen(buf), "redirect_uri", 
							redirect_uri, sizeof(redirect_uri));
	if (!(oa2_grant_find(code, &g) &&
				g.status == GRANTED_STATUS &&
				g.validity > now &&
				strcmp(g.client_id, client_id) == 0 &&
				g.redirect_uri && strcmp(g.redirect_uri, redirect_uri) == 0)) {
		send_response(conn, 400, "Bad Request", "invalid_grant", NULL);
		return;
	}
	if (mkstr(token, sizeof(token), MKSTR_TOKEN) &&
			oa2_token_store(token, now+c.token_validity, 0, client_id, 
							g.user_id, g.scope) &&
			(c.refresh_token_validity < 0 || 
			 (mkstr(refresh_token, sizeof(refresh_token), MKSTR_TOKEN) &&
				oa2_token_store(refresh_token, now+c.refresh_token_validity, 1, 
								client_id, g.user_id, g.scope)))) {
		oa2_grant_delete(code);
		send_response_200(conn, token, c.token_validity, refresh_token, NULL);
	}
	else
		send_response(conn, 500, "Internal Server Error",
										"server_error", "Cannot store token");
}

/*
 * Resource Owner password credentials
 */

static void handle_password_token(struct http_connection *conn, const char *buf)
{
	char username[ITEMLEN],
			 password[ITEMLEN], 
			 client_id[IDLEN], 
			 client_secret[PASSWORDLEN],
			 scope[ITEMLEN],
			 token[TOKENLEN],
			 refresh_token[TOKENLEN];	
	struct oa2_client c;
	int status;
	time_t now = time(NULL);

	http_trace(conn, "handle_password_token [%s]", buf);

	if (!(http_get_var(buf, strlen(buf), "username",
									 username, sizeof(username)) > 0 &&
				http_get_var(buf, strlen(buf), "password",
									 password, sizeof(password)) > 0 &&
				get_client_credentials(conn, buf, 
												client_id, sizeof(client_id),
												client_secret, sizeof(client_secret)))) { 
		send_response(conn, 400, "Bad Request", "invalid_request", NULL);
		return;
	}
	if (!oa2_client_find(client_id, &c)) {
		send_response(conn, 400, "Bad Request", "invalid_client", NULL);
		return;
	}
	if (c.auth != PASSWORD_AUTH) {
		send_response(conn, 400, "Bad Request", "unauthorized_client", NULL);
		return;
	}
	if ((c.confidential || c.secret) &&
			!(enforce_client_credentials(conn, client_id, client_secret, c.secret))) {
		send_response_401(conn);
		return;
	}
	memset(client_secret, 0, sizeof(client_secret));
	if (http_get_var(buf, strlen(buf), "scope", scope, sizeof(scope)) > 0 &&
			!is_valid_scope(scope)) {
		send_response(conn, 400, "Bad Request", "invalid_scope", NULL);
		return;
	}
	if ((status = check_user(conn, username, password)) != STATUS_OK) {
		send_response(conn, 400, "Bad Request",
										"access_denied", check_user_status(status));
		return;
	}
	memset(password, 0, sizeof(password));
	if (mkstr(token, sizeof(token), MKSTR_TOKEN) &&
			oa2_token_store(token, now+c.token_validity, 0, client_id, 
							username, scope) &&
			(c.refresh_token_validity < 0 ||
			 (mkstr(refresh_token, sizeof(refresh_token), MKSTR_TOKEN) &&
				oa2_token_store(refresh_token, now+c.refresh_token_validity, 1, 
							client_id, username, scope))))
		send_response_200(conn, token, c.token_validity, refresh_token, scope);
	else
		send_response(conn, 500, "Internal Server Error",
										"server_error", "Cannot store token");
}

/*
 * Client credentials
 */

static void handle_client_token(struct http_connection *conn, const char *buf)
{
	char client_id[IDLEN], 
			 client_secret[PASSWORDLEN],
			 scope[ITEMLEN],
			 token[TOKENLEN],
			 refresh_token[TOKENLEN];	
	struct oa2_client c;
	time_t now = time(NULL);

	http_trace(conn, "handle_client_token [%s]", buf);

	if (!get_client_credentials(conn, buf, 
											client_id, sizeof(client_id),
											client_secret, sizeof(client_secret))) {
		send_response(conn, 400, "Bad Request", "invalid_request", NULL);
		return;
	}
	if (!oa2_client_find(client_id, &c)) {
		send_response(conn, 400, "Bad Request", "invalid_client", NULL);
		return;
	}
	if (c.auth != CLIENT_AUTH) {
		send_response(conn, 400, "Bad Request", "unauthorized_client", NULL);
		return;
	}
	if (!enforce_client_credentials(conn, client_id, client_secret, c.secret)) {
		send_response_401(conn);
		return;
	}
	memset(client_secret, 0, sizeof(client_secret));
	if (http_get_var(buf, strlen(buf), "scope", scope, sizeof(scope)) > 0 &&
			!is_valid_scope(scope)) {
		send_response(conn, 400, "Bad Request", "invalid_scope", NULL);
		return;
	}
	if (mkstr(token, sizeof(token), MKSTR_TOKEN) &&
			oa2_token_store(token, now+c.token_validity, 0,
							client_id, NULL, scope))
		send_response_200(conn, token, c.token_validity, NULL, scope);
	else
		send_response(conn, 500, "Internal Server Error",
										"server_error", "Cannot store token");
}

/*
 * Refresh token access
 */

static void handle_refresh_token(struct http_connection *conn, const char *buf)
{
	char refresh_token[TOKENLEN], 
			 client_id[IDLEN], 
			 client_secret[PASSWORDLEN],
			 scope[ITEMLEN],
			 token[TOKENLEN];	
	struct oa2_client c;
	struct oa2_token t;
	time_t now = time(NULL);

	http_trace(conn, "handle_refresh_token [%s]", buf);

	if (!(http_get_var(buf, strlen(buf), "refresh_token",
									 refresh_token, sizeof(refresh_token)) > 0)) {
		send_response(conn, 400, "Bad Request", "invalid_request", NULL);
		return;
	}
	if (!(get_client_credentials(conn, buf, 
												client_id, sizeof(client_id),
												client_secret, sizeof(client_secret)) &&
				oa2_client_find(client_id, &c))) {
		send_response(conn, 400, "Bad Request", "invalid_client", NULL);
		return;
	}
	if (!(oa2_token_find(refresh_token, &t) &&
				t.refresh &&
				(t.validity == 0 || t.validity > now) &&
				strcmp(t.client_id, client_id) == 0)) {
		send_response(conn, 400, "Bad Request", "invalid_grant", NULL);
		return;
	}
	if ((c.confidential || c.secret) &&
			!(enforce_client_credentials(conn, client_id, client_secret, c.secret))) {
		send_response_401(conn);
		return;
	}
	memset(client_secret, 0, strlen(client_secret));
	if (http_get_var(buf, strlen(buf), "scope", scope, sizeof(scope)) > 0 &&
			!enforce_scope((&t)->scope, scope)) {
		send_response(conn, 400, "Bad Request", "invalid_scope", NULL);
		return;
	}
	if (mkstr(token, sizeof(token), MKSTR_TOKEN) &&
			oa2_token_store(token, now+c.token_validity, 0, 
							client_id, t.user_id, scope))
		send_response_200(conn, token, c.token_validity, NULL, scope);
	else
		send_response(conn, 500, "Internal Server Error",
										"server_error", "Cannot store token");
}

/*
 * The "tokeninfo" endpoint.
 */

#ifdef TOKENINFO
int oauth2_tokeninfo(struct http_connection *conn)
{
	struct http_request *req = http_get_request(conn);
	char access_token[TOKENLEN], dbuf[DATELEN];
	struct oa2_token t;
	time_t now = time(NULL);
	int i;

	http_trace(conn, "tokeninfo [%s]", 
							req->query_string ? req->query_string : "");

	if (!(strcmp(req->method, "GET") == 0 &&
				req->query_string &&
				http_get_var(req->query_string, strlen(req->query_string),
								"access_token", access_token, sizeof(access_token)) > 0 &&
				oa2_token_find(access_token, &t) &&
				t.validity > now)) {
		send_response(conn, 400, "Bad Request", "invalid_request", NULL);
		return 1;
	}

	http_printf(conn,
			"HTTP/1.1 200 OK\r\n"
			"Server: %s\r\nDate: %s\r\n"
			"Content-Type: application/json; charset=UTF-8\r\n"
			"Cache-Control: no-store\r\nPragma: no-cache\r\n\r\n"
			"{\r\n\"access_token\":\"%s\",\r\n\"user_id\":\"%s\",\r\n"
			"\"scope\":\"%s\",\r\n\"expires_in\":%ld\r\n}\r\n",
			http_get_config(conn)->server_software,
			date(dbuf, sizeof(dbuf), 0),
			access_token,
			t.user_id,
			t.scope, 
			t.validity - now);
	return 1;
}
#endif

int oauth2_token(struct http_connection *conn)
{
	struct http_request *req = http_get_request(conn);
	char buf[BUFLEN], grant_type[CODELEN];

	if (strcmp(req->method, "POST") == 0 &&
			http_read(conn, buf, sizeof(buf)) &&
	 		http_get_var(buf, strlen(buf), "grant_type", 
									 grant_type, sizeof(grant_type)) > 0) {
		if (strcmp(grant_type, "authorization_code") == 0)
			handle_code_token(conn, buf);
		else if (strcmp(grant_type, "password") == 0)
			handle_password_token(conn, buf);
		else if (strcmp(grant_type, "client_credentials") == 0)
			handle_client_token(conn, buf);
		else if (strcmp(grant_type, "refresh_token") == 0)
			handle_refresh_token(conn, buf);
		else
			send_response(conn, 400, "Bad Request", "unsupported_grant_type", NULL);
	}
	else
		send_response(conn, 400, "Bad Request", "invalid_request", NULL);
	return 1;
}
