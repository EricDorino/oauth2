/*
 * Copyright (C) 2016,2018 Éric Dorino
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Register an oauth2 client
 */

#include <sys/stat.h>
#include <crypt.h>
#include <errno.h>
#include <getopt.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "global.h"

#define DEFAULT_GRANT_VALIDITY		"600"
#define DEFAULT_TOKEN_VALIDITY		"3600"
#define DEFAULT_RTOKEN_VALIDITY		"7200"
#define MIN_GRANT_VALIDITY				300
#define MIN_TOKEN_VALIDITY				600
#define MIN_RTOKEN_VALIDITY				3600
#define MAX_GRANT_VALIDITY				1200
#define MAX_TOKEN_VALIDITY				86400
#define MAX_RTOKEN_VALIDITY				86400

const char *grant[] = {
	"code",
	"implicit",
	"password",
	"client"
};

/*
const char *grant_text[] = {
	"Authorization code grant",
	"Implicit grant",
	"Resource owner password credentials grant",
	"Client credentials grant"
};
*/

#define OPTS 		"a:chpu:t:V"

#define	GRANT_VALIDITY					256
#define	TOKEN_VALIDITY					257
#define	REFRESH_TOKEN_VALIDITY	258

static struct option opt[] = {
	{"auth", required_argument, NULL, 'a'},
	{"authentication", required_argument, NULL, 'a'},
	{"confidential", no_argument, NULL, 'c'},
	{"grant-validity", required_argument, NULL, GRANT_VALIDITY},
	{"help", no_argument, NULL, 'h'},
	{"public", no_argument, NULL, 'p'},
	{"redirect-uri", required_argument, NULL, 'u'},
	{"uri", required_argument, NULL, 'u'},
	{"refresh-token-validity", required_argument, NULL, REFRESH_TOKEN_VALIDITY},
	{"token-validity", required_argument, NULL, TOKEN_VALIDITY},
	{"type", required_argument, NULL, 't'},
	{"version", no_argument, NULL, 'V'},
	{NULL, 0, NULL, 0}
};

static struct option_help {
	int val;
	const char *text;
	const char *arg;
	const char *posvalue[4];
	const char *defvalue[4];
} opt_help[] = {
	{'a', "Specify an authentication method", "method",
					{"code", "implicit", "password", "client"},
				 	{"code", NULL, NULL, NULL}},
	{'c', "Mark this client as confidential (same as --type confidential)", NULL, 
					{NULL, NULL, NULL, NULL}, 
					{NULL, NULL, NULL, NULL}},
	{'h', "Show this help and exit", NULL, 
					{NULL, NULL, NULL, NULL}, 
					{NULL, NULL, NULL, NULL}},
	{'p', "Mark this client as public (same as --type public)", NULL, 
					{NULL, NULL, NULL, NULL}, 
					{NULL, NULL, NULL, NULL}},
	{'t', "Type of the client", "type", 
					{"confidential", "public", NULL, NULL}, 
					{NULL, NULL, NULL, NULL}},
	{'u', "Redirection callback URI", "URI", 
					{NULL, NULL, NULL, NULL}, 
					{NULL, NULL, NULL, NULL}},
	{'V', "Show the program version and exit", NULL, 
					{NULL, NULL, NULL, NULL}, 
					{NULL, NULL, NULL, NULL}},
	{GRANT_VALIDITY, "Grant ticket validity, in seconds", "delay", 
					{NULL, NULL, NULL, NULL}, 
					{DEFAULT_GRANT_VALIDITY, DEFAULT_GRANT_VALIDITY, NULL, NULL}},
	{TOKEN_VALIDITY, "Token validity, in seconds", "delay", 
					{NULL, NULL, NULL, NULL}, 
					{DEFAULT_TOKEN_VALIDITY, DEFAULT_TOKEN_VALIDITY, 
						DEFAULT_TOKEN_VALIDITY, DEFAULT_TOKEN_VALIDITY}},
	{REFRESH_TOKEN_VALIDITY, "Refresh token validity, in seconds", "delay", 
					{NULL, NULL, NULL, NULL}, 
					{DEFAULT_RTOKEN_VALIDITY, NULL, DEFAULT_RTOKEN_VALIDITY, NULL}},
	{0, NULL, NULL, {NULL, NULL, NULL, NULL}, {NULL, NULL, NULL, NULL}}
};

static void usage(void)
{
	int i = 0, j = 0, k;

  fputs("usage: oauth2_register [<option>]* [<client_id>]\n", stderr);
  fputs("       oauth2_register -h\n", stderr);
  fputs("       oauth2_register -V\n", stderr);
	fputs("\noptions:\n", stderr);

	while (opt[i].val) {
		k = 0;
		while (opt_help[k].val  && opt_help[k].val != opt[i].val)
			k++;
		if (opt[i].val < 127) {
			if (opt[i].has_arg)
				fprintf(stderr, " -%c <%s>, ", opt[i].val, opt_help[k].arg);
			else
				fprintf(stderr, " -%c, ", opt[i].val);
		}
		else
			fputs(" ", stderr);
		j = i;
		while(opt[j].val == opt[i].val) {
			if (opt[i].has_arg)
				fprintf(stderr, "--%s <%s>", opt[j].name, opt_help[k].arg);
			else
				fprintf(stderr, "--%s", opt[j].name);
			j++;
			if (opt[j].val != opt[i].val)
				fputs(":\n", stderr);
			else 
				fputs(", ", stderr);
		}	
		fprintf(stderr, "      %s.", opt_help[k].text);
		if (opt_help[k].posvalue[0]) {
			int x;

			fputs("\n      Valid values are: ", stderr);
			for (x = 0; x < 4 && opt_help[k].posvalue[x]; x++) {
				fprintf(stderr, "%s", opt_help[k].posvalue[x]);
				if (x+1 < 4 && opt_help[k].posvalue[x+1])
					fputs(", ", stderr);
			}
		}
		if (opt_help[k].defvalue[0] && 
				!opt_help[k].defvalue[1] &&
				!opt_help[k].defvalue[2] &&
				!opt_help[k].defvalue[3])
			fprintf(stderr, " (Default is %s).", opt_help[k].defvalue[0]);
		else if (opt_help[k].defvalue[0] ||
						 opt_help[k].defvalue[1] ||
						 opt_help[k].defvalue[2] ||
						 opt_help[k].defvalue[3]) {
			int m = 0, n;
			while (opt_help[m].val && opt_help[m].val != 'a')
				m++;
			fputs("(Default is ", stderr);
			for (n = 0; n < 4; n++)
				if (opt_help[k].defvalue[n]) {
					fprintf(stderr, "%s (%s)", 
									opt_help[k].defvalue[n],
									opt_help[m].posvalue[n]);
					if ((n+1 < 4 && opt_help[k].defvalue[n+1]) ||
							(n+2 < 4 && opt_help[k].defvalue[n+2]) ||
							(n+3 < 4 && opt_help[k].defvalue[n+3]))
						fputs(", ", stderr);
				}
			fputs(").", stderr);
		}
		fputs("\n", stderr);
		i = j;
	}
	fputs("\nFor confidential client, a secret is written on stdout. \n", stderr);
	fputs("This secret is only stored in encrypted form on the ", stderr);
	fputs("server.\n", stderr);

  exit(EXIT_FAILURE);
}

static int generate_secret(char *pass, size_t psize,
													char *epass, size_t epsize)
{
	char rand[32], salt[32];

	if (!mkstr(pass, psize, MKSTR_PASSWORD)) {
		fprintf(stderr, "oauth2: %s\n", strerror(errno));
		return 0;
	}
	if (!mkstr(rand, sizeof(rand), MKSTR_SALT)) {
		fprintf(stderr, "oauth2: %s\n", strerror(errno));
		return 0;
	}
	sprintf(salt, "$6$%s$", rand);
	strncpy(epass, crypt(pass, salt), epsize);
	return 1;
}

int main(int argc, char **argv) {
	int c, confidential = 0, public = 0, nuris = 0, auth = NO_AUTH;
	long grant_val = -2, token_val = -2, rtoken_val = -2;
	char *client_id = NULL, *redirect_uri[NURIS],	idbuf[20],
			 client_secret[64], secret[128];
	struct oa2_client cl;

	memset(redirect_uri, 0, NURIS*sizeof(char *));
	memset(client_secret, 0, sizeof(client_secret));
	memset(secret, 0, sizeof(secret));
	
	opterr = 0;
	for (;;) {
		int index = 0;

		if ((c = getopt_long(argc, argv, OPTS, opt, &index)) < 0)
			break;

		switch (c) {
			default:
				usage();
				break;
			case 'a':
				if (auth)
					usage();
				else {
					int i;

					auth = NO_GRANT;
					for (i = 0; i < 4; i++)
						if (strcmp(optarg, grant[i]) == 0) {
							auth = i + 1;
							break;
						}
				}
				if (!auth) {
					fputs("oauth2_register: invalid authentication method\n", stderr);
					return EXIT_FAILURE;
				}
				break;
			case 'c':
				if (confidential || public) {
					usage();
				}
				confidential = 1;
				break;
			case 't':
				if (confidential || public) {
					usage();
				}
				if (strcmp(optarg, "confidential") == 0)
					confidential = 1;
				else if (strcmp(optarg, "public") == 0)
					public = 1;
				else
					usage();
				break;
			case 'p':
				if (confidential || public)
					usage();
				public = 1;
				break;
			case 'u':
				if (nuris >= NURIS) {
					fputs("oauth2_register: too many URIs\n", stderr);
					return EXIT_FAILURE;
				}
				if (!oa2_check_uri(optarg)) {
					fputs("oauth2_register: invalid URI\n", stderr);
					return EXIT_FAILURE;
				}
				redirect_uri[nuris++] = oa2_encode(optarg);
				break;
			case GRANT_VALIDITY:
				if (grant_val > 0)
					usage();
				grant_val = atol(optarg);
				if (grant_val < MIN_GRANT_VALIDITY || grant_val > MAX_GRANT_VALIDITY) {
					fputs("oauth2_register: invalid delay\n", stderr);
					return EXIT_FAILURE;
				}
				break;
			case TOKEN_VALIDITY:
				if (token_val > 0)
					usage();
				token_val = atol(optarg);
				if (token_val < MIN_TOKEN_VALIDITY || token_val > MAX_TOKEN_VALIDITY) {
					fputs("oauth2_register: invalid delay\n", stderr);
					return EXIT_FAILURE;
				}
				break;
			case REFRESH_TOKEN_VALIDITY:
				if (rtoken_val > 0)
					usage();
				rtoken_val = atol(optarg);
				if (rtoken_val < MIN_RTOKEN_VALIDITY ||
						rtoken_val > MAX_RTOKEN_VALIDITY) {
					fputs("oauth2_register: invalid delay\n", stderr);
					return EXIT_FAILURE;
				}
				break;
			case 'h':
				usage();
				break;
			case 'V':
				fprintf(stderr, "oauth2_register, part of %s/%s\n\n",
					PACKAGE_NAME, PACKAGE_VERSION);
				fputs("Copyright (C) 2016,2018  Éric Dorino\n"
					"This is free software; see the source "
					"for copying conditions.  There is NO\n"
					"warranty; not even for MERCHANTABILITY "
					"of FITNESS FOR A PARTICULAR PURPOSE.\n",
					stderr);
				return EXIT_FAILURE;
		}
	}
	if (optind == argc - 1)
		client_id = argv[optind];
	else
		usage();	 /* We need a client_id */
	

	if ((confidential && public) || (!confidential && !public)) {
		fputs("register oauth2: client must be confidential or public\n", stderr);
		return EXIT_FAILURE;
	}

	/*
	 * Default values
	 */

	if (auth == NO_AUTH)
		auth = CODE_AUTH;

	if (auth == CODE_AUTH) {
		if (grant_val < -1)
			grant_val = atoi(DEFAULT_GRANT_VALIDITY);
		if (token_val < -1)
			token_val = atoi(DEFAULT_TOKEN_VALIDITY);
		if (rtoken_val < -1)
			rtoken_val = atoi(DEFAULT_RTOKEN_VALIDITY);
	}
	else if (auth == IMPLICIT_AUTH) {
		if (grant_val < -1)
			grant_val = atoi(DEFAULT_GRANT_VALIDITY);
		if (token_val < -1)
			token_val = atoi(DEFAULT_TOKEN_VALIDITY);
		if (rtoken_val > 0) {
			fputs("oauth2_register: "
						"no refresh token for implicit authentication\n", stderr);
			return EXIT_FAILURE;
		}
		rtoken_val = -1;
	}
	else if (auth == PASSWORD_AUTH) {
		if (grant_val < -1)
			grant_val = -1;
		if (token_val < -1)
			token_val = atoi(DEFAULT_TOKEN_VALIDITY);
		if (rtoken_val < -1)
			rtoken_val = atoi(DEFAULT_RTOKEN_VALIDITY);
	}
	else { /* CLIENT_AUTH */
		if (grant_val < -1)
			grant_val = -1;
		if (token_val < -1)
			token_val = atoi(DEFAULT_TOKEN_VALIDITY);
		if (rtoken_val > 0) {
			fputs("oauth2_register: "
						"no refresh token for client authentication\n", stderr);
			return EXIT_FAILURE;
		}
		rtoken_val = -1;
	}

	if (confidential) {
		if (!generate_secret(client_secret, sizeof(client_secret),
												 secret, sizeof(secret))) {
			fputs("oauth2_register: cannot generate secret\n", stderr);
			return EXIT_FAILURE;
		}
	}

	if (strlen(client_id) >= IDLEN) {
		fputs("oauth2_register: client_id is too long\n", stderr);
		return EXIT_FAILURE;
	}

	if (!oa2_client_store(client_id, secret, confidential, auth,
													grant_val, token_val, rtoken_val,
													redirect_uri)) {
		fprintf(stderr, "oauth2_register: cannot register: %s\n", 
						strerror(errno));
		return EXIT_FAILURE;
	}

	if (confidential)
		puts(client_secret);

  return EXIT_SUCCESS;
}
