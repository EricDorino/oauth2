/*
 * Copyright (C) 2016  Éric Dorino.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <ctype.h>
#include <httpserver.h>
#include <locale.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "gettext.h"
#include "global.h"

#define _(str) gettext(str)

/*
 * The Consent form.
 * This is shown to the Resource Owner, which may accept (giving credentials),
 * or deny.
 */

struct lang {
	char lc[4];		/* language code */
	char cc[4];		/* country code */
	int qval;
};

static int lang_compare(const void *a, const void *b)
{
	return ((struct lang *)b)->qval - ((struct lang *)a)->qval;
}

const char *choose_locale(void)
{
	#define MAXLANG	10

	struct lang lang[MAXLANG];
	char *al = getenv("HTTP_ACCEPT_LANGUAGE"), *p, *q, *r;
	static char loc[32];
	int n = 0;

	memset(lang, 0, MAXLANG*sizeof(struct lang));

	if (!al)
		return "";

	while ((p = strsep(&al, ",")) != NULL) {
		if (n > MAXLANG || strcmp(p, "*") == 0)
			continue;

		strncpy(lang[n].lc, p, 3);
	
		if ((r = index(p, '-')) != NULL) {
			*r++ = '\0';
			strncpy(lang[n].cc, r, 3);
		}
		else
			strncpy(lang[n].cc, r = p, 3);

		if ((q = index(r, ';')) != NULL) {
			*q++ = '\0';
			if (*q == 'q' && *(q+1) == '=') {
				lang[n].qval = (int)(atof(q+2)*10.0);
				if (lang[n].qval > 0) /* get rid of lang when qval = 0 */
					n++;
			}
			else
				lang[n++].qval = 10;
		}
		else
			lang[n++].qval = 10;
	}
	if (n == 0)
		return "";

	if (n > 1)
		qsort(lang, n, sizeof(struct lang), lang_compare);

	for(p = lang[0].cc; p && *p; p++)
		*p = toupper(*p);

	snprintf(loc, sizeof(loc), "%s%s%s.UTF-8",
						lang[0].lc,
						lang[0].cc ? "_" : "",
						lang[0].cc ? lang[0].cc : "");
	return loc;
}

int main(void)
{
	const char *query_string = getenv("QUERY_STRING");
	char grant[64], title[64], text1[512], text2[512];
	struct oa2_grant g;
	struct oa2_client c;
	time_t now = time(NULL);

	setlocale(LC_ALL, choose_locale());
	bindtextdomain(PACKAGE, LOCALEDIR);
	textdomain(PACKAGE);

	if (!(query_string &&
				http_get_var(query_string, strlen(query_string), "grant",
							grant, sizeof(grant)) > 0)) {
		puts("Status: 400 Bad Request\n");
		return EXIT_SUCCESS;
	}
	if (!(oa2_grant_find(grant, &g) && 
				now < g.validity &&
				g.status == WAIT_CODE_STATUS &&
				oa2_client_find(g.client_id, &c))) {
		puts("Status: 400 Bad Request\n");
		return EXIT_SUCCESS;
	}
	snprintf(title, sizeof(title),
		_("Server %s Consent Form"), getenv("SERVER_NAME"));

	snprintf(text1, sizeof(text1),
		_("The %s client application <b>%s</b> request access to your "
		"resources."),
		c.confidential ? _("confidential") : _("public"),
		g.client_id);

	snprintf(text2, sizeof(text2),
			_("You can either accept, granting access for the specified scope "
			"by entering your credentials, or deny access."));

	printf(
"Status: 200 OK\n"
"Content-Type: text/html; charset=utf-8\n\n"
"<?xml version=\"1.0\" encoding=\"utf-8\" ?>"
"<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\""
"\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">"
"<html xmlns=\"http://www.w3.org/1999/xhtml\" lang=\"en\">"
"<head>"
	"<meta name=\"Author\" content=\"Éric Dorino\" />"
	"<meta name=\"Version\" content=\"%s\" />"
	"<title>%s</title>"
	"<link rel=\"stylesheet\" "
		"href=\"/oauth2/css/style.css\" type=\"text/css\" />"
	"<meta http-equiv=\"content-type\" "
		"content=\"text/html; charset=utf-8\" />"
"</head>"
"<body>"
	"<div id=\"header\" class=\"header\">%s</div>"
	"<div id=\"main\" class=\"main\">"
		"<div id=\"main_header\" class=\"main_header\">"
			"%s<p />" /*text1*/
			"<ul>"
				"<li>%s: <b>%s</b><p /></li>"
				"<li>%s: <b>%s</b><p /></li>"
				"<li>%s: <b>&quot;%s&quot;</b></li>"
			"</ul><p />"
			"%s<p />" /*text2*/
		"</div>"
		"<div id=\"deny_box\" class=\"box\">"
			"<form id=\"deny_form\" action=\"/oauth2/consent_deny\" method=\"POST\">"
				"<input type=\"hidden\" name=\"grant\" value=\"%s\" />"
				"<input id=\"deny_button\" type=\"submit\" value=\"%s\" />"
			"</form>"
		"</div>"
		"<div id=\"accept_box\" class=\"box\">"
			"<form id=\"accept_form\" action=\"/oauth2/consent_accept\" method=\"POST\">"
				"<input type=\"text\" id=\"username\" name=\"username\""
											"placeholder=\"%s\" /> </p>"
				"<input type=\"password\" id=\"password\" name=\"password\" "
											"placeholder=\"%s\" /> </p>"
				"<input type=\"hidden\" name=\"grant\" value=\"%s\" />"
				"<div id=\"accept_button_container\">"
					"<input id=\"accept_button\" type=\"submit\" value=\"%s\" />"
				"</div>"
			"</form>"
		"</div>"
	"</div>"
"<div id=\"footer\" class=\"footer\"> copyright &copy; 2016 Éric Dorino.<br />"
	"<div id=\"footer_background\" class=\"background\"> </div>"
"</div>"
"</body>"
"</html>",
		getenv("SERVER_SOFTWARE"),
		title,
		title,
		text1,
		_("Client"), g.client_id,
		_("Client type"), c.confidential ? _("confidential") : _("public"),
		_("Scope"), g.scope ? g.scope : "",
		text2,
		grant,
		_("Deny"),
		_("User name"),
		_("Password"),
		grant,
		_("Accept"));
	return EXIT_SUCCESS;
}
